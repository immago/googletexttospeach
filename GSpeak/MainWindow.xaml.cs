﻿/*
 *                          GSpeak
 * 
 * This application demonstrates Google text-to-speach capabilities.
 * 
 * Url example: http://translate.google.com/translate_tts?tl=en&q="hello%20world" returns mp3 file.
 * Where "hello%20world" is your text and "en" is lang.
 * Restrictions and features: 
 *      - Maximum 100 letters
 *      - Need internet conection
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;

namespace GSpeak
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MediaPlayer player = new MediaPlayer();

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// OK button press event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {

            //lang
            string lang = "";
            if (radiobuttonRu.IsChecked.Value)
                lang = "ru";

            if (radiobuttonEn.IsChecked.Value)
                lang = "en";

            //url
            string inputText = textBoxInput.Text;
            string encstr = string.Empty;
            encstr = Uri.EscapeDataString(inputText);
            string url = "http://translate.google.com/translate_tts?tl=" + lang + "&q=" + encstr;

            buttonOK.IsEnabled = false;

            //create folder at user appdata if need
            string filePath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GSpeakTmp");
            bool exists = System.IO.Directory.Exists(filePath);
            if (!exists)
                System.IO.Directory.CreateDirectory(filePath);
            
            string filePathFull = System.IO.Path.Combine(filePath, "text.mp3");

            //download
            bool success = downloadFile(url, filePathFull, 5);

            //play file
            if (success)
            {
                playFile(filePathFull);
            }else
            {
                MessageBox.Show("Error while donloading");
                buttonOK.IsEnabled = true;
            }
        }

        /// <summary>
        /// Play file at filePath
        /// </summary>
        /// <param name="filePath">Path of file</param>
        void playFile(string filePath)
        {
            player.Open(new Uri(filePath, UriKind.Relative));
            player.Play();
            player.MediaEnded += new EventHandler(delegate(Object o, EventArgs a)
            {
                player.Close();
                buttonOK.IsEnabled = true;
            });
            
        }

        /// <summary>
        /// Download file from url
        /// </summary>
        /// <param name="url">url of file</param>
        /// <param name="filePath">save file to this path</param>
        /// <param name="maxTrys">trys count</param>
        /// <returns>Successfully</returns>
        bool downloadFile(string url, string filePath, int maxTrys)
        {
            int tryConunter = 0;
            while (tryConunter <= maxTrys)
            {
                try
                {
                    WebClient web = new WebClient();
                    web.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/4.0 (compatible; MSIE 9.0; Windows;)");
                    web.DownloadFile(url, filePath);

                    return true;
                }
                catch (Exception ex) { };
                System.Threading.Thread.Sleep(200);
                tryConunter++;
            }

            return false;
        }
    }
}
