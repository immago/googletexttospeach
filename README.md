GSpeak
  
This application demonstrates Google text-to-speach capabilities.
 
Url example: http://translate.google.com/translate_tts?tl=en&q="hello%20world" returns mp3 file.
Where "hello%20world" is your text and "en" is lang.

Restrictions and features: 

- Maximum 100 letters

- Need internet conection